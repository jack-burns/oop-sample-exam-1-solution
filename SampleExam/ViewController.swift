//
//  ViewController.swift
//  SampleExam
//
//  Created by Chris Chadillon on 2016-03-08.
//  Copyright © 2016 Chris Chadillon. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let MAX_ROWS = 8
    let MAX_COLS = 8
    let MIN_ROWS = 1
    let MIN_COLS = 1
    
    @IBOutlet weak var rowPlus: UIBarButtonItem!
    @IBOutlet weak var rowMinus: UIBarButtonItem!
    @IBOutlet weak var colPlus: UIBarButtonItem!
    @IBOutlet weak var colMinus: UIBarButtonItem!
    @IBOutlet weak var theDrawingView: DrawingView!
    
    @IBAction func rowPlusClicked(sender: UIBarButtonItem) {
        theDrawingView.numRows++
        if ( theDrawingView.numRows == MAX_ROWS) {
            rowPlus.enabled = false
        } else {
            rowMinus.enabled = true
        }
        theDrawingView.initArrayOfBools()
        theDrawingView.setNeedsDisplay()
    }
    
    @IBAction func rowMinusClicked(sender: UIBarButtonItem) {
        theDrawingView.numRows--
        if ( theDrawingView.numRows == MIN_ROWS ) {
            rowMinus.enabled = false
        } else {
            rowPlus.enabled = true
        }
        theDrawingView.initArrayOfBools()
        theDrawingView.setNeedsDisplay()
    }
    
    @IBAction func colPlusClicked(sender: UIBarButtonItem) {
        theDrawingView.numCols++
        if ( theDrawingView.numCols == MAX_COLS ) {
            colPlus.enabled = false
        } else {
            colMinus.enabled = true
        }
        theDrawingView.initArrayOfBools()
        theDrawingView.setNeedsDisplay()
    }
    
    @IBAction func colMinusClicked(sender: UIBarButtonItem) {
        theDrawingView.numCols--
        if ( theDrawingView.numCols == MIN_COLS ) {
            colMinus.enabled = false
        } else {
            colPlus.enabled = true
        }
        theDrawingView.initArrayOfBools()
        theDrawingView.setNeedsDisplay()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        theDrawingView.initArrayOfBools()
    }
}

