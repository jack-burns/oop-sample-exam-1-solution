//
//  DrawingView.swift
//  SampleExam
//
//  Created by Chris Chadillon on 2016-03-08.
//  Copyright © 2016 Chris Chadillon. All rights reserved.
//

import UIKit

class DrawingView: UIView {

    let BORDER = 20
    var numRows = 1
    var numCols = 1
    var arrayOfRects = [[CGRect]]()
    var filledRects = [[Bool]]()
    
    override func drawRect(rect: CGRect) {
        
        arrayOfRects = [[CGRect]]()
        
        let context = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, 1.0)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let lineComps: [CGFloat] = [0.0, 0.0, 1.0, 1.0]
        let fillComps: [CGFloat] = [1.0, 0.0, 0.0, 1.0]
        
        let lineColor = CGColorCreate(colorSpace, lineComps)
        let fillColor = CGColorCreate(colorSpace, fillComps)
        
        CGContextSetStrokeColorWithColor(context, lineColor)
        CGContextSetFillColorWithColor(context, fillColor)
        
        let widthOfBoard = Int(self.bounds.width)-BORDER*2
        let heightOfBoard = Int(self.bounds.height)-BORDER*2
        
        let rowHeight = heightOfBoard/numRows
        let colWidth = widthOfBoard/numCols
        
        let xPos = BORDER
        let yPos = BORDER

        var YP = yPos
        for (var row = 0; row<numRows; row++) {
            arrayOfRects.append([CGRect]())
            var XP = xPos
            for (var col = 0; col<numCols; col++) {
                let rect = CGRect(x:XP, y:YP, width: colWidth, height: rowHeight)
                arrayOfRects[row].append(rect)
                
                if filledRects[row][col] == true {
                    CGContextAddRect(context, rect)
                    CGContextFillPath(context)
                }
                
                CGContextAddRect(context, rect)
                CGContextStrokePath(context)
                XP+=colWidth
            }
            YP+=rowHeight
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first! as UITouch
        let location = touch.locationInView(self)
        
        for (var row = 0; row<numRows; row++) {
            for (var col = 0; col<numCols; col++) {
                if (CGRectContainsPoint(arrayOfRects[row][col], CGPoint(x: location.x, y: location.y))) {
                    filledRects[row][col] = !filledRects[row][col]
                }
            }
        }
        self.setNeedsDisplay()
    }
    
    func initArrayOfBools() {
        
        filledRects = [[Bool]]()
        
        for (var row = 0; row<numRows; row++) {
            filledRects.append([Bool]())
            for (var col = 0; col<numCols; col++) {
                filledRects[Int(row)].append(false);
            }
        }
    }

}
